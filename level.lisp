;;codierung:
;;0 = leeres Feld
;;1 = frucht
;;2 = Schlange

;;Variablen

(defparameter *fieldlength* 25)
(setf *fieldlength* 25)
(defparameter *level* '())
(defparameter *fruitposition* '(nil nil))
(setf *fruitposition* '(nil nil))

;;erstellt das Spielfeld, bestehend aus einer Liste aus Listen, quasi ein zweidimensionales Array
(defun create-level (length level)
  (dotimes (i length level)
    (setf level (cons (fill-list length) level))))

;;debugging
;;(setf *level* (create-level *fieldlength* *level*))

;;liefert eine zufällige Koordinate zwischen 0 und er angegebenen Länge
(defun rand-index (length)
  (list (random length) (random length)))

;;platziert eine Frucht auf dem Spielfeld an der angegebenen Stelle
(defun add-fruit (level index)
  (setf (nth (first index) (nth (second  index) level)) 1)
  (setf *fruitposition* index))

;;Helferfunktion um das Spielfeld zu erstellen
(defun fill-list (length)
  (let ((dummy-list '()))
    (dotimes (i length dummy-list)
      (setf dummy-list (cons 0 dummy-list)))))

;;Zeichnet das Level mithilfe von lispbuilder-sdl,  eventuell auslagern
(defun draw-level (level)
  (list nil nil)
  )
