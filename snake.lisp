;;Variablen
(defparameter *snake '())
(defparameter *direction '(0 0))

(defun initialize-snake (position snake field)
  setf snake (cons '((/ (list-length fiel) 2) (/ (list-length field) 2)) snake))

;;bewege die schlange, ohne sie zu verlängern. 
(defun move-snake (snake direction)
  (setf snake (cons (add-positions direction (first snake)) snake))
  (setf snake (reverse (cdr (reverse snake)))))


;;addiert 2 koordinaten
(defun add-positions (pos1 pos2)
  (mapcar #'+ pos1 pos2))

;;bewegt die schlange und verlängert sie
(defun move-and-add-snake (snake direction)
  (setf snake (cons (add-positions direction (first snake)) snake)))
